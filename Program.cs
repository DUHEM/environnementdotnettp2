﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP2.Entities;
using TP2.Utils;

namespace TP2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            foreach (var item in FakeDb.Instance.Users)
            {
                Console.WriteLine(item);
            }

            #region Q1
            Console.WriteLine("Question 1");
            Console.WriteLine(Question1());

            #endregion
            #region Q2
            Console.WriteLine("Question 2");
            var q2 = Question2();
            DisplayResult(q2);
            #endregion
            #region Q3
            Console.WriteLine("Question 3");
            var q3 = Question3();
            DisplayResult(q3);

            //#endregion
            //#region Q4
            Console.WriteLine("Question 4");
            var q4 = Question4();
            Console.WriteLine(q4);
            #endregion
            #region Q5
            Console.WriteLine("Question 5");
            var q5 = Question5();
            DisplayResult(q5);
            #endregion
            #region Q6
            Console.WriteLine("Question 6");
            var q6 = Question6();
            DisplayResult(q6);
            #endregion
            #region Q7
            Console.WriteLine("Question 7");
            var q7 = Question7();
            DisplayResult(q7);

            #endregion
            Console.ReadKey();
        }
        public static int Question1()
        {
            // Afficher le nombre de personne s'appelant Dupond ou Dupont.
            return FakeDb.Instance.Users.Where(x => x.Lastname.Equals("Dupond") || x.Lastname.Equals("Dupont")).Count();
        }

        public static IEnumerable<User> Question2()
        {
            // Afficher les personnes enregistré avec le role Automobiliste.
            return FakeDb.Instance.Users.Where(x => x.Roles.Any(p => p.Name.Equals("Automobiliste")));
        }

        public static IEnumerable<dynamic> Question3()
        {
            // Afficher les plaques d'immatriculation de toutes les voitures (une seule fois par voiture) liées à au moins un utilisateur.

            return FakeDb.Instance.Users.SelectMany(x => x.Cars.Select(y => y.Registration)).Distinct();
        }

        public static Car Question4()
        {
            // Afficher la ou les voiture(s) avec le plus de kilomètre

            return FakeDb.Instance.Cars.OrderByDescending(x => x.Mileage).First();
        }

        public static IEnumerable<dynamic> Question5()
        {
            // Afficher le classement des types de voiture par nombre de voiture unique présentes du plus grand au plus petit.

            return FakeDb.Instance.Cars.Select(x => x.Type).Distinct();
        }

        public static IEnumerable<dynamic> Question6()
        {
            // Afficher les "Garagiste" liés à 4 voitures ou plus.

            return FakeDb.Instance.Users.Where(x => x.Roles.Any(p => p.Name.Equals("Garagiste"))).Where(y => y.Cars.Count >= 4);
        }

        public static IEnumerable<dynamic> Question7()
        {
            // Afficher les "Controlleur" et la liste des voitures aux quelles ils sont liés.

            return FakeDb.Instance.Users.Where(x => x.Roles.Any(p => p.Name.Equals("Controlleurs"))).SelectMany(y => y.Cars);
        }
        public static void DisplayResult(IEnumerable<dynamic> result)
        {
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("\n");
        }
    }
}
